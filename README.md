# HajStation3 Coldboot for PS3

It's the PS3 coldboot but with Blahaj instead. Incredible!

## Creating a Modified coldboot.raf

I used [Berion's coldboot tool on PSX Place](https://www.psx-place.com/resources/coldboot-injector.1315/) to extract images and to inject the new images back into `coldboot.raf`.
GIMP was used to edit the images. There's a copy of my modified `coldboot.raf` under the [releases](https://gitlab.com/4096kb/ps3hajcoldboot/-/releases) on this repo.

## Assets Used:

 - Font: [SCE-PS3 Rodin LATIN](https://www.onlinewebfonts.com/download/3c9a33e9913448d684afff5b4b0cc59c)
 - Base Images: Extracted from original PS3 `coldboot.raf` with MD5 `FBFD819D6CD834AC2BDDA02EE3D5375F`

## License

I'm not affiliated with Sony or Ikea in any shape or form and thus won't be attaching a license to this repo. If you're a 
Sony or Ikea legal assassin, please don't hurt me I will take down the repo if you ask :(

## Disclaimer

I'm not responsible if your PS3 is damaged in any form or fashion after applying this coldboot file. If things break, that's a you problem. Reinstall with PS3UPDAT.PUP.
